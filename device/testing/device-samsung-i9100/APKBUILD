# Reference: <https://postmarketos.org/devicepkg>
pkgname=device-samsung-i9100
pkgver=4
pkgrel=0
pkgdesc="Samsung Galaxy SII"
url="https://postmarketos.org"
arch="armv7"
license="MIT"
depends="postmarketos-base libsamsung-ipc"
makedepends="devicepkg-dev"
install="$pkgname.post-install"
options="!check !archcheck"
subpackages="
	$pkgname-kernel-downstream:kernel_downstream
	$pkgname-kernel-mainline:kernel_mainline
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-weston:weston
"
source="
	deviceinfo
	90-android-touch-dev.rules
	weston.ini
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
	install -D -m644 "$srcdir"/90-android-touch-dev.rules \
		"$pkgdir"/etc/udev/rules.d/90-android-touch-dev.rules
}

kernel_downstream() {
	pkgdesc="Downstream kernel"
	depends="linux-samsung-i9100 mesa-dri-swrast"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_mainline() {
	pkgdesc="Close to mainline kernel"
	depends="linux-postmarketos-exynos4 mesa-dri-gallium mesa-egl"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

nonfree_firmware() {
	pkgdesc="Wifi firmware"
	depends="firmware-samsung-i9100"
	mkdir "$subpkgdir"
}

weston() {
	install_if="$pkgname-kernel-mainline weston"
	install -Dm644 "$srcdir"/weston.ini \
		"$subpkgdir"/etc/xdg/weston/weston.ini
}

sha512sums="fbd32402a31e132fbe25fd03572a62069d7b592c738821279941c8ca799e2489096e77faad8741f3bbacbb2999dfaed756e43bdfd1b76112c29b29b872d87d30  deviceinfo
089635daddd88eec35451bfe98dc3713035e3623c896dd21305b990ecf422e8fbb54e010cf347919bbb3a7385f639ab119280477fe0783df3228168d97d96fc6  90-android-touch-dev.rules
de794566118f1744d068a94e6a75b61d43f6749a4b0871a5270fa7a2048164d609c71fcffa61845c2a7dd4cb5fbeb72c0e4f8b73b382f36d6ff0bcc9b8a5ae25  weston.ini"

# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qmlkonsole
pkgver=0_git20210122
pkgrel=0
_commit="48dad8a56d0b44b7c71e63b31652b4ce3ad64db5"
pkgdesc="Terminal app for Plasma Mobile"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/plasma-mobile/qmlkonsole"
license="GPL-3.0-or-later"
depends="
	qmltermwidget
	kirigami2
	"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	"
source="https://invent.kde.org/plasma-mobile/qmlkonsole/-/archive/$_commit/qmlkonsole-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="301b2a6f94de067158001872932e031f6ff4a96debee5d97f761a5195186a1951e1eda47653083f35f96dc51e514599d61bbf6f010799d9992c121e997b75603  qmlkonsole-48dad8a56d0b44b7c71e63b31652b4ce3ad64db5.tar.gz"
